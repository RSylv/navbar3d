const hamburger_menu = document.querySelector('.hamburger-menu')
const container = document.querySelector('.container')
const main = document.querySelector('.main')

hamburger_menu.addEventListener('click', () => {
    container.classList.toggle('active')
    
    if (container.classList.contains('active')) {
        main.addEventListener('click', () => {            
            container.classList.toggle('active', false)
        })
    } 
})